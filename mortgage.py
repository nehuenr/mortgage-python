import matplotlib.pyplot as plt



def calculateYearsToPay (annuity, price, interest_rate):

	if annuity < price * interest_rate: annuity = price * interest_rate + 1


	years = 1
	leftToPay = price
	g_leftToPay = []
	totalPaid = 0

	while leftToPay > 0:

		g_leftToPay.append (leftToPay)

		leftToPay -= annuity
		totalPaid += annuity

		leftToPay *= 1 + interest_rate

		years += 1

	years -= 1
	totalPaid += -leftToPay
	g_leftToPay [-1] = 0


	leftToPay = price
	g_leftToPayIF = []

	while leftToPay > 0:

		g_leftToPayIF.append (leftToPay)

		leftToPay -= annuity

	g_leftToPayIF.append (0)


	return years, g_leftToPay, g_leftToPayIF, totalPaid, annuity


rent = 50000
exchange_rate = 160
annuity = (rent / exchange_rate) * 12						# loan monthly payment equal to the rent
price = 90000
interest_rate = 0.02

years, g_leftToPay, g_leftToPayIF, totalPaid, annuity = calculateYearsToPay (annuity, price, interest_rate)

print ("Monthly Payment: " + str (round (annuity / 12)))
print ("Annuity: " + str (annuity))
print ("Years: " + str (years))
print ("Principal: " + str (price))
print ("Annuity to principal ratio: " + str (round (annuity / price, 2)))
print ("Total paid: " + str (round (totalPaid)))
print ("Interest: " + str (round (totalPaid - price)))
print ("Interest rate: " + str (interest_rate))

plt.plot (range (len (g_leftToPay)), g_leftToPay)			# with interest
plt.plot (range (len (g_leftToPayIF)), g_leftToPayIF)		# interest free
plt.show()
















